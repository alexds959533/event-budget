from logging.config import fileConfig

from alembic import context
from sqlalchemy import create_engine

from event_budget.infrastructure import orm
from settings import WebAppSettings

config = context.config


fileConfig(config.config_file_name)

target_metadata = [orm.metadata]

# в случае тестов скрипт запускается внутри питона и средствами alembic внутрь прокидывается контекст
# который уже содержит url
try:
    URL = config.cmd_opts.pg_url
except AttributeError:
    class DbSettings(WebAppSettings):
        class Config:
            env_file = "local.env"
    URL = DbSettings().postgres_dsn.replace("+asyncpg", "")


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """

    context.configure(
        url=URL,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = create_engine(URL)

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
