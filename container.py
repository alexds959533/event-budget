from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Factory, Singleton

from settings import WebAppSettings
from event_budget.infrastructure.connection import AsyncSQLAlchemy
from event_budget.infrastructure.unit_of_work import SqlAlchemyUnitOfWork
from event_budget.service.add_event.use_case import AddEventUseCase
from event_budget.service.add_members.use_case import AddMembersUseCase
from event_budget.service.add_purchases.use_case import AddPurchaseUseCase
from event_budget.service.get_transactions.use_case import GetTransactionsUseCase
from event_budget.service.get_event.use_case import GetEventsUseCase
from event_budget.service.exclude_members.use_case import ExcludeMembersUseCase


class Container(DeclarativeContainer):
    settings = Singleton(WebAppSettings)
    db = Singleton(AsyncSQLAlchemy, settings.provided.postgres_dsn)

    # Unit Of Work
    unit_of_work = Factory(SqlAlchemyUnitOfWork, engine=db.provided.engine)

    # Use Case
    add_event_use_case = Factory(AddEventUseCase, uow=unit_of_work)
    add_members_use_case = Factory(AddMembersUseCase, uow=unit_of_work)
    add_purchases_use_case = Factory(AddPurchaseUseCase, uow=unit_of_work)
    get_transactions_use_case = Factory(GetTransactionsUseCase, uow=unit_of_work)
    get_event_use_case = Factory(GetEventsUseCase, uow=unit_of_work)
    exclude_members_use_case = Factory(ExcludeMembersUseCase, uow=unit_of_work)
