from event_budget.errors import PurchaseNotFound, EventNotFound
from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.dto import PurchaseDTO
from event_budget.service.exclude_members.command import ExcludeMembersCommand


class ExcludeMembersUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    @property
    def uow(self) -> AbstractUnitOfWork:
        return self._uow

    async def __call__(self, command: ExcludeMembersCommand) -> PurchaseDTO:
        async with self._uow as uow:
            event = await uow.events.get_by_purchase_id(command.purchase_id)
            if not event:
                raise EventNotFound()
            purchase = event.get_purchase_by_id(command.purchase_id)
            if not purchase:
                raise PurchaseNotFound()

            for member_name in command.members_names:
                member = event.get_member_by_name(member_name)
                if member:
                    purchase.exclude(member.id)

            await uow.commit()
            await uow.refresh(event)
            return purchase.to_dto()
