from uuid import UUID

from pydantic import BaseModel


class ExcludeMembersCommand(BaseModel):
    purchase_id: UUID
    members_names: list[str]
