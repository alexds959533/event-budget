from event_budget.domain.model import Purchase
from event_budget.errors import MemberNotFound
from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.add_purchases.command import AddPurchaseCommand
from event_budget.service.dto import MemberDTO


class AddPurchaseUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    @property
    def uow(self) -> AbstractUnitOfWork:
        return self._uow

    async def __call__(self, command: AddPurchaseCommand) -> MemberDTO:
        async with self._uow as uow:
            event = await uow.events.get_by_member_id(command.member_id)
            if not event:
                raise MemberNotFound()
            member = event.get_member_by_id(command.member_id)
            if not member:
                raise MemberNotFound()

            for purchase_dto in command.purchases:
                purchase = Purchase.create(purchase_dto)
                member.add_purchase(purchase)
            await uow.commit()
            await uow.refresh(event)
            return member.to_dto()
