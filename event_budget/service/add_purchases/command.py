from uuid import UUID

from pydantic import BaseModel

from event_budget.service.dto import CreatePurchaseDTO


class AddPurchaseCommand(BaseModel):
    member_id: UUID
    purchases: list[CreatePurchaseDTO]
