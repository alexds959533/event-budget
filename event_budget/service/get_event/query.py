from uuid import UUID

from pydantic import BaseModel


class GetEventQuery(BaseModel):
    event_id: UUID
