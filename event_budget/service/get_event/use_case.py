from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.dto import EventDTO
from event_budget.service.get_event.query import GetEventQuery
from event_budget.errors import EventNotFound


class GetEventsUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    async def __call__(self, query: GetEventQuery) -> EventDTO:
        async with self._uow as uow:
            event = await uow.events.get(query.event_id)
            if not event:
                raise EventNotFound
            return event.to_dto()
