from event_budget.domain.model import Member
from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.add_members.command import AddMembersCommand
from event_budget.service.dto import EventDTO
from event_budget.errors import EventNotFound


class AddMembersUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    @property
    def uow(self) -> AbstractUnitOfWork:
        return self._uow

    async def __call__(self, command: AddMembersCommand) -> EventDTO:
        async with self._uow as uow:
            event = await uow.events.get(command.event_id)
            if not event:
                raise EventNotFound
            for member_dto in command.members:
                member = Member.create(member_dto)
                event.add_member(member)
            await uow.commit()
            await uow.refresh(event)
            return event.to_dto()
