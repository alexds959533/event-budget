from uuid import UUID

from pydantic import BaseModel

from event_budget.service.dto import CreateMemberDTO


class AddMembersCommand(BaseModel):
    event_id: UUID
    members: list[CreateMemberDTO]
