import datetime
from uuid import UUID

from pydantic import BaseModel


class CreatePurchaseDTO(BaseModel):
    name: str
    price: float
    description: str | None = None
    exclude_members: list[UUID] | None


class CreateMemberDTO(BaseModel):
    name: str


class CreateEventDTO(BaseModel):
    name: str
    planing_date: datetime.date | None = None
    description: str | None = None


class PurchaseDTO(BaseModel):
    id: UUID
    name: str
    price: float
    description: str | None = None
    exclude_members: list[UUID]


class MemberDTO(BaseModel):
    id: UUID
    name: str
    purchases: list[PurchaseDTO]
    spend: float


class EventDTO(BaseModel):
    id: UUID
    name: str
    planing_date: datetime.date | None = None
    description: str | None = None
    budget: float
    member_count: float
    members: list[MemberDTO]


class TransactionDTO(BaseModel):
    sender: str
    receiver: str
    amount: float
