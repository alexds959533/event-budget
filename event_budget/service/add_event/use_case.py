from event_budget.domain.model import Event
from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.add_event.command import AddEventCommand
from event_budget.service.dto import EventDTO


class AddEventUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    @property
    def uow(self) -> AbstractUnitOfWork:
        return self._uow

    async def __call__(self, command: AddEventCommand) -> EventDTO:
        async with self._uow as uow:
            event = Event.create(command.event)
            uow.events.add(event)
            await uow.commit()
            await uow.refresh(event)
            return event.to_dto()
