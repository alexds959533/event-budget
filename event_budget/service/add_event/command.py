from pydantic import BaseModel

from event_budget.service.dto import CreateEventDTO


class AddEventCommand(BaseModel):
    event: CreateEventDTO
