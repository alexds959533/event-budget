from uuid import UUID

from pydantic import BaseModel


class GetTransactionsQuery(BaseModel):
    event_id: UUID
