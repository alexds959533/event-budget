from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.dto import TransactionDTO
from event_budget.service.get_transactions.query import GetTransactionsQuery
from event_budget.errors import EventNotFound


class GetTransactionsUseCase:
    def __init__(self, uow: AbstractUnitOfWork) -> None:
        self._uow = uow

    async def __call__(self, query: GetTransactionsQuery) -> list[TransactionDTO]:
        async with self._uow as uow:
            event = await uow.events.get(query.event_id)
            if not event:
                raise EventNotFound
            return event.get_transactions()
