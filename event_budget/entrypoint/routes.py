from uuid import UUID

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends

from container import Container
from event_budget.service import dto
from event_budget.service.add_event.use_case import AddEventCommand, AddEventUseCase
from event_budget.service.add_members.use_case import AddMembersCommand, AddMembersUseCase
from event_budget.service.add_purchases.use_case import AddPurchaseCommand, AddPurchaseUseCase
from event_budget.service.exclude_members.use_case import ExcludeMembersCommand, ExcludeMembersUseCase
from event_budget.service.get_event.use_case import GetEventQuery, GetEventsUseCase
from event_budget.service.get_transactions.use_case import GetTransactionsQuery, GetTransactionsUseCase

api_router = APIRouter()


@api_router.post("/event/", response_model=dto.EventDTO)
@inject
async def add_event(
    command: AddEventCommand, add_event_use_case: AddEventUseCase = Depends(Provide[Container.add_event_use_case])
) -> dto.EventDTO:
    event = await add_event_use_case(command)
    return event


@api_router.post("/members/", response_model=dto.EventDTO)
@inject
async def add_members(
    command: AddMembersCommand,
    add_members_use_case: AddMembersUseCase = Depends(Provide[Container.add_members_use_case]),
) -> dto.EventDTO:
    event = await add_members_use_case(command)
    return event


@api_router.post("/purchases/", response_model=dto.MemberDTO)
@inject
async def add_purchases(
    command: AddPurchaseCommand,
    add_purchases_use_case: AddPurchaseUseCase = Depends(Provide[Container.add_purchases_use_case]),
) -> dto.MemberDTO:
    member = await add_purchases_use_case(command)
    return member


@api_router.post("/purchases/member_exclude", response_model=dto.PurchaseDTO)
@inject
async def member_exclude(
    command: ExcludeMembersCommand,
    exclude_members_use_case: ExcludeMembersUseCase = Depends(Provide[Container.exclude_members_use_case]),
) -> dto.PurchaseDTO:
    purchase = await exclude_members_use_case(command)
    return purchase


@api_router.get("/transaction/{event_id}", response_model=list[dto.TransactionDTO])
@inject
async def get_transactions(
    event_id: UUID,
    get_transactions_use_case: GetTransactionsUseCase = Depends(Provide[Container.get_transactions_use_case]),
) -> list[dto.TransactionDTO]:
    transactions = await get_transactions_use_case(GetTransactionsQuery(event_id=event_id))
    return transactions


@api_router.get("/event/{event_id}", response_model=dto.EventDTO)
@inject
async def get_event(
    event_id: UUID, get_event_ese_case: GetEventsUseCase = Depends(Provide[Container.get_event_use_case])
) -> dto.EventDTO:
    event = await get_event_ese_case(GetEventQuery(event_id=event_id))
    return event
