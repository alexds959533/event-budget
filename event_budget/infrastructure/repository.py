import abc
from uuid import UUID

from sqlalchemy import select

from event_budget.domain.model import Event, Member, Purchase
from event_budget.infrastructure import orm


class AbstractRepository(abc.ABC):
    def add(self, event: Event) -> None:
        self._add(event)

    async def get(self, event_id: UUID) -> Event | None:
        event = await self._get(event_id)
        return event

    async def get_by_member_id(self, member_id: UUID) -> Event | None:
        event = await self._get_by_member_id(member_id)
        return event

    async def get_by_purchase_id(self, purchase_id: UUID) -> Event | None:
        event = await self._get_by_purchase_id(purchase_id)
        return event

    @abc.abstractmethod
    def _add(self, event: Event) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def _get(self, event_id: UUID) -> Event | None:
        raise NotImplementedError

    @abc.abstractmethod
    async def _get_by_member_id(self, member_id: UUID) -> Event | None:
        raise NotImplementedError

    @abc.abstractmethod
    async def _get_by_purchase_id(self, purchase_id: UUID) -> Event | None:
        raise NotImplementedError


class SqlAlchemyRepository(AbstractRepository):
    def __init__(self, session):
        super().__init__()
        self.session = session

    def _add(self, event: Event) -> None:
        self.session.add(event)

    async def _get(self, event_id: UUID) -> Event | None:
        result = await self.session.execute(select(Event).where(orm.event.c.id == event_id))
        return result.scalars().first()

    async def _get_by_member_id(self, member_id: int) -> Event | None:
        result = await self.session.execute(select(Event).join(Member).where(orm.member.c.id == member_id))
        return result.scalars().first()

    async def _get_by_purchase_id(self, purchase_id: UUID) -> Event | None:
        result = await self.session.execute(
            select(Event).join(Member).join(Purchase).where(orm.purchase.c.id == purchase_id)
        )
        return result.scalars().first()
