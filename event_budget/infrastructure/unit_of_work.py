import abc

from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession

from event_budget.domain.model import Event
from event_budget.infrastructure import repository


class AbstractUnitOfWork(abc.ABC):
    events: repository.AbstractRepository

    async def __aenter__(self) -> "AbstractUnitOfWork":
        return self

    async def __aexit__(self, *args):
        await self.rollback()

    async def commit(self):
        await self._commit()

    @abc.abstractmethod
    async def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def rollback(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def refresh(self, event: Event):
        raise NotImplementedError

    @abc.abstractmethod
    async def flush(self):
        raise NotImplementedError


class SqlAlchemyUnitOfWork(AbstractUnitOfWork):
    def __init__(self, engine: AsyncEngine):
        self._engine = engine
        self._session: AsyncSession | None = None

    @property
    def engine(self) -> AsyncEngine:
        assert self._engine is not None
        return self._engine

    @property
    def session(self) -> AsyncSession:
        assert self._session is not None
        return self._session

    async def __aenter__(self):
        self._session = AsyncSession(self.engine)
        self.events = repository.SqlAlchemyRepository(self.session)
        return await super().__aenter__()

    async def __aexit__(self, *args):
        await super().__aexit__(*args)
        await self.session.close()

    async def _commit(self):
        await self.session.commit()

    async def rollback(self):
        await self.session.rollback()

    async def refresh(self, event: Event):
        await self.session.refresh(event)

    async def flush(self):
        await self.session.flush()
