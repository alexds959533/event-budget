from sqlalchemy import Column, Date, Float, ForeignKey, MetaData, String, Table, Text
from sqlalchemy.dialects.postgresql import ARRAY, UUID
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.orm import registry, relationship

from event_budget.domain.model import Event, Member, Purchase

metadata = MetaData()

purchase = Table(
    "purchase",
    metadata,
    Column("id", UUID(as_uuid=True), primary_key=True),
    Column("name", String),
    Column("price", Float),
    Column("description", Text, nullable=True),
    Column("member_id", UUID(as_uuid=True), ForeignKey("member.id")),
    Column("exclude_members", ARRAY(UUID(as_uuid=True), dimensions=1), nullable=False, default=MutableList([])),
)

member = Table(
    "member",
    metadata,
    Column("id", UUID(as_uuid=True), primary_key=True),
    Column("name", String),
    Column("event_id", UUID(as_uuid=True), ForeignKey("event.id")),
)

event = Table(
    "event",
    metadata,
    Column("id", UUID(as_uuid=True), primary_key=True),
    Column("name", String),
    Column("date", Date, nullable=True),
    Column("description", Text, nullable=True),
)


def start_mappers() -> None:
    mapper_registry = registry()
    mapper_registry.map_imperatively(Purchase, purchase)
    mapper_registry.map_imperatively(
        Member,
        member,
        properties={"purchases": relationship(Purchase, backref="member", collection_class=list, lazy="joined")},
    )
    mapper_registry.map_imperatively(
        Event,
        event,
        properties={
            "members": relationship(Member, backref="event", collection_class=list, lazy="joined"),
        },
    )
