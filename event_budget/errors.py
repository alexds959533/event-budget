class ModelError(Exception):
    pass


class EventNotFound(ModelError):
    pass


class MemberNotUnique(ModelError):
    pass


class MemberNotFound(ModelError):
    pass


class PurchaseNotFound(ModelError):
    pass
