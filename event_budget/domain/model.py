import uuid
from dataclasses import dataclass, field
from datetime import date
from typing import Optional
from uuid import UUID

from event_budget import errors
from event_budget.service.dto import (
    CreateEventDTO,
    CreateMemberDTO,
    CreatePurchaseDTO,
    EventDTO,
    MemberDTO,
    PurchaseDTO,
    TransactionDTO,
)


@dataclass
class Purchase:
    id: UUID
    name: str
    price: float
    exclude_members: list[UUID] = field(default_factory=list)
    description: str | None = None
    member: Optional["Member"] = None

    @classmethod
    def create(cls, data: CreatePurchaseDTO) -> "Purchase":
        purchase_id = uuid.uuid4()
        return cls(id=purchase_id, **data.dict())

    @property
    def purchase_price_per_person(self):
        """Prise for purchase divided on count member that used it"""
        return self.price / (self.member.event.member_count - len(self.exclude_members))

    def exclude(self, member: UUID) -> None:
        self.exclude_members.append(member)
        self.exclude_members = self.exclude_members

    def to_dto(self) -> PurchaseDTO:
        return PurchaseDTO(
            id=self.id,
            name=self.name,
            price=self.price,
            description=self.description,
            exclude_members=self.exclude_members,
        )


@dataclass(unsafe_hash=True)
class Member:
    id: UUID
    name: str
    purchases: list[Purchase] = field(default_factory=list)
    user_id: int | None = None
    event: Optional["Event"] = None

    def __gt__(self, other: "Member"):
        if self.get_money_to_transfer() is None:
            return False
        if other.get_money_to_transfer() is None:
            return True
        return self.get_money_to_transfer() > other.get_money_to_transfer()

    @classmethod
    def create(cls, data: CreateMemberDTO) -> "Member":
        member_id = uuid.uuid4()
        return cls(id=member_id, **data.dict())

    def __post_init__(self):
        """Back link purchase with member"""
        for purchase in self.purchases:
            purchase.member = self

    def add_purchase(self, purchase: Purchase) -> None:
        self.purchases.append(purchase)
        purchase.member = self

    @property
    def total_member_spending(self) -> float:
        """Total price that user paid for the event"""
        return sum(purchase.price for purchase in self.purchases)

    @property
    def total_cost_per_member(self):
        """Total price that user should pay for the event"""
        return sum(
            purchase.purchase_price_per_person
            for purchase in self.event.purchases
            if self not in purchase.exclude_members
        )

    def get_money_to_transfer(self) -> float:
        return self.total_member_spending - self.total_cost_per_member

    def to_dto(self) -> MemberDTO:
        return MemberDTO(
            id=self.id,
            name=self.name,
            purchases=[purchase.to_dto() for purchase in self.purchases],
            spend=self.total_member_spending,
        )


@dataclass
class Event:
    id: UUID
    name: str
    members: list[Member] = field(default_factory=list)
    planing_date: date | None = None
    description: str | None = None

    def __post_init__(self):
        for member in self.members:
            member.event = self

    def __hash__(self):
        return hash(self.id)

    @classmethod
    def create(cls, data: CreateEventDTO) -> "Event":
        event_id = uuid.uuid4()
        return cls(id=event_id, **data.dict())

    @property
    def member_count(self) -> int:
        return len(self.members)

    @property
    def budget(self) -> float:
        return sum(member.total_member_spending for member in self.members)

    def add_member(self, member: Member) -> None:
        existing_names = {member.name for member in self.members}
        if member.name in existing_names:
            raise errors.MemberNotUnique()
        self.members.append(member)
        member.event = self

    def get_member_by_id(self, member_id: UUID) -> Member | None:
        return next((member for member in self.members if member.id == member_id), None)

    def get_member_by_name(self, name: str) -> Member | None:
        return next((member for member in self.members if member.name == name), None)

    def get_purchase_by_id(self, purchase_id: UUID) -> Purchase | None:
        return next(
            (purchase for member in self.members for purchase in member.purchases if purchase.id == purchase_id), None
        )

    @property
    def purchases(self) -> list[Purchase]:
        purchases = []
        for member in self.members:
            purchases.extend(member.purchases)
        return purchases

    def to_dto(self) -> EventDTO:
        return EventDTO(
            id=self.id,
            name=self.name,
            planing_date=self.planing_date,
            description=self.description,
            member_count=self.member_count,
            budget=self.budget,
            members=[member.to_dto() for member in self.members],
        )

    def get_transactions(self) -> list[TransactionDTO]:
        """Method that count transaction after event"""

        transactions = []
        sorted_member = sorted(self.members)
        member_map = dict(enumerate(sorted_member))
        balances = [member.get_money_to_transfer() for member in sorted_member]

        for i, balance in enumerate(balances):

            while balance < 0:
                max_positive_balance = max(balances)
                j = balances.index(max_positive_balance)
                amount = min(-balance, max_positive_balance)

                transactions.append(
                    TransactionDTO(sender=member_map[i].name, receiver=member_map[j].name, amount=amount)
                )

                balance += amount
                balances[i] += amount
                balances[j] -= amount

        return transactions
