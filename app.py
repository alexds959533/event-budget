import uvicorn
from fastapi import FastAPI
from container import Container
from sqlalchemy.orm import clear_mappers

from settings import WebAppSettings
from event_budget.entrypoint import routes
from event_budget.infrastructure.orm import start_mappers

SERVICE_NAME = "event_budget"

app = FastAPI(title=SERVICE_NAME)
app.include_router(routes.api_router, prefix="/api/v1")

container = Container()
app.container = container
db = container.db()
container.wire(modules=[routes])


@app.on_event("startup")
async def on_startup():
    await db.connect(echo=True)
    start_mappers()


@app.on_event("shutdown")
async def on_shutdown():
    clear_mappers()
    await db.disconnect()


if __name__ == "__main__":
    settings = WebAppSettings()
    uvicorn.run(
        f"app:app",
        port=settings.port,
        reload=True
    )
