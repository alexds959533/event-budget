# Event-budget Management Service

This service provides an API for managing events budget and their participants.

## Description

The service allows you to add events, members, purchases, and count necessary money transfer between members 

## Description

The service allows you to add events, members, purchases, and count necessary money transfer between members 

## Patterns and arch
- repository
- unit of work
- ddd

## Endpoints

### 1. Add Event

**URL:** `/api/event/`

**Method:** `POST`

**Description:** Add a new event.

**Parameters:**
- `command` (AddEventCommand, required) - Command for adding an event.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON object with information about the added event in the format of EventDTO.

### 2. Add Members

**URL:** `/api/members/`

**Method:** `POST`

**Description:** Add new members to an event.

**Parameters:**
- `command` (AddMembersCommand, required) - Command for adding members.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON object with information about the event in the format of EventDTO with the added members.

### 3. Add Purchases

**URL:** `/api/purchases/`

**Method:** `POST`

**Description:** Add a new purchase for a member.

**Parameters:**
- `command` (AddPurchaseCommand, required) - Command for adding a purchase.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON object with information about the member in the format of MemberDTO with the added purchase.

### 4. Exclude Members from Event

**URL:** `/api/purchases/member_exclude`

**Method:** `POST`

**Description:** Exclude specified members from an event.

**Parameters:**
- `command` (ExcludeMembersCommand, required) - Command for excluding members.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON object with information about the purchase in the format of PurchaseDTO.

### 5. Get Transactions

**URL:** `/api/transaction/{event_id}`

**Method:** `GET`

**Description:** Get a list of transactions for the specified event.

**Parameters:**
- `event_id` (UUID, required) - Event identifier.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON array with information about transactions in the format of TransactionDTO.

### 6. Get Event Information

**URL:** `/api/event/{event_id}`

**Method:** `GET`

**Description:** Get information about an event by its identifier.

**Parameters:**
- `event_id` (UUID, required) - Event identifier.

**Successful Response:**
- Code: 200 OK
- Response Body: JSON object with information about the event in the format of EventDTO.
