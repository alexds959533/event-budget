from pydantic import PositiveInt, BaseSettings, PostgresDsn


class WebAppSettings(BaseSettings):
    port: PositiveInt = 8002
    postgres_dsn: PostgresDsn

    class Config:
        env_file = "local.env"


settings = WebAppSettings()
