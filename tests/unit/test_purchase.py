import uuid

import pytest

from event_budget.domain.model import Event, Purchase


def test_exclude_member(big_party: Event):
    # todo сейчас в покупке можно исключать любого члена - даже не связанного с событием, нужно добавить валидацию
    purchase = Purchase(name="A", price=10, id=uuid.uuid4())
    purchase.exclude_member([big_party.members[-1]])
    assert purchase.exclude_members == [big_party.members[-1]]


@pytest.mark.parametrize(
    "purchase,exclude_members_cnt,price_per_person",
    [
        (Purchase(name="drinks", price=1000, id=uuid.uuid4()), 0, 200),
        (Purchase(name="drinks", price=1000, id=uuid.uuid4()), 1, 250),
        (Purchase(name="drinks", price=1000, id=uuid.uuid4()), 3, 500),
        (Purchase(name="drinks", price=1000, id=uuid.uuid4()), 4, 1000),
    ],
)
def test_purchase_price_per_person(
    big_party: Event, purchase: Purchase, exclude_members_cnt: int, price_per_person: float
):
    members_to_exclude = big_party.members[:exclude_members_cnt]
    purchase.exclude_member(members_to_exclude)
    big_party.members[-1].add_purchase(purchase=purchase)
    assert purchase.purchase_price_per_person == price_per_person
