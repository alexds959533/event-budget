import uuid

import pytest

from event_budget.domain.model import Event, Member, Purchase


@pytest.mark.parametrize(
    "member,total_member_spending",
    [
        (Member(name="Alex", id=uuid.uuid4()), 0),
        (Member(name="Alex", id=uuid.uuid4(), purchases=[Purchase(name="A", price=10, id=uuid.uuid4())]), 10),
        (
            Member(
                name="Alex",
                id=uuid.uuid4(),
                purchases=[Purchase(name="A", price=10, id=uuid.uuid4()), Purchase(name="B", price=2, id=uuid.uuid4())],
            ),
            12,
        ),
    ],
)
def test_total_member_spending(member: Member, total_member_spending: float):
    assert member.total_member_spending == total_member_spending


def test_member_add_purchase(big_party: Event):
    member = Member(
        name="Alex",
        purchases=[
            Purchase(name="A", price=10, id=uuid.uuid4()),
            Purchase(name="B", price=2, id=uuid.uuid4()),
            Purchase(name="C", price=30, id=uuid.uuid4()),
        ],
        id=uuid.uuid4(),
    )
    member.add_purchase(Purchase(name="D", price=50, id=uuid.uuid4()))
    assert member.total_member_spending == 92
    assert len(member.purchases) == 4


def test_member_should_pay(big_party: Event):
    for member in big_party.members:
        assert member.total_cost_per_member == 1000

    # member[0] doesn't drink
    big_party.members[-1].add_purchase(
        purchase=Purchase(name="drinks", price=1000, exclude_members=[big_party.members[0]], id=uuid.uuid4())
    )
    assert big_party.members[0].total_cost_per_member == 1000
    for member in big_party.members[1:]:
        assert member.total_cost_per_member == 1250


def test_member_get_money_to_transfer(big_party: Event):
    expected = [-700, 0, 150, -1000, 1550]
    for member, expected in zip(big_party.members, expected):
        assert member.get_money_to_transfer() == expected


def test_members_sort(big_party: Event):
    expected = [-1000, -700, 0, 150, 1550]
    for member, expected in zip(sorted(big_party.members), expected):
        assert member.get_money_to_transfer() == expected
