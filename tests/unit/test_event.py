import uuid

import pytest

from event_budget.domain.model import Event, Member, Purchase
from event_budget.service.dto import TransactionDTO

PARTY_PURCHASE_INIT = [
    Purchase(name="A", price=10, id=uuid.uuid4()),
    Purchase(name="B", price=20, id=uuid.uuid4()),
    Purchase(name="C", price=30, id=uuid.uuid4()),
]


@pytest.fixture
def party() -> Event:
    purchase_1_member_1 = PARTY_PURCHASE_INIT[0]
    purchase_2_member_1 = PARTY_PURCHASE_INIT[1]
    purchase_1_member_3 = PARTY_PURCHASE_INIT[2]

    member_1 = Member(name="Alex", purchases=[purchase_1_member_1, purchase_2_member_1], id=uuid.uuid4())
    member_2 = Member(name="Kirill", id=uuid.uuid4())
    member_3 = Member(name="Rama", purchases=[purchase_1_member_3], id=uuid.uuid4())

    return Event(id=uuid.uuid4(), name="new year", members=[member_1, member_2, member_3])


@pytest.fixture
def party_empty() -> Event:
    return Event(id=uuid.uuid4(), name="new year")


@pytest.mark.parametrize("party_fixture,budget", [("party", 60), ("party_empty", 0), ("big_party", 5000)])
def test_party_budget(request, party_fixture: str, budget: float):
    party = request.getfixturevalue(party_fixture)
    assert party.budget == budget


@pytest.mark.parametrize("party_fixture,member_count", [("party", 3), ("party_empty", 0), ("big_party", 5)])
def test_party_member_count(request, party_fixture: str, member_count: int):
    party = request.getfixturevalue(party_fixture)
    assert party.member_count == member_count


@pytest.mark.parametrize(
    "party_fixture,purchases",
    [
        ("party", PARTY_PURCHASE_INIT),
        ("party_empty", []),
    ],
)
def test_party_purchases(request, party_fixture: str, purchases: list[Purchase]):
    party = request.getfixturevalue(party_fixture)
    assert party.purchases == purchases


def test_party_add_member(party: Event):
    member = Member(
        name="Romа",
        id=uuid.uuid4(),
        purchases=[Purchase(name="D", price=40, id=uuid.uuid4()), Purchase(name="E", price=50, id=uuid.uuid4())],
    )
    party.add_member(member)

    assert member.event == party
    assert party.budget == 150
    assert party.member_count == 4


def test_get_transactions(big_party: Event):
    """user4 send 1000 to user5, user1 send 550 to user5, user1 send 150 to user3"""
    expected = [
        TransactionDTO(sender=big_party.members[3].name, receiver=big_party.members[4].name, amount=1000.0),
        TransactionDTO(sender=big_party.members[0].name, receiver=big_party.members[4].name, amount=550.0),
        TransactionDTO(sender=big_party.members[0].name, receiver=big_party.members[2].name, amount=150.0),
    ]
    actual = big_party.get_transactions()
    assert expected == actual
