from uuid import UUID, uuid4

import pytest

from event_budget.domain.model import Event
from event_budget.errors import MemberNotFound, MemberNotUnique
from event_budget.infrastructure.repository import AbstractRepository
from event_budget.infrastructure.unit_of_work import AbstractUnitOfWork
from event_budget.service.add_event.use_case import AddEventCommand, AddEventUseCase
from event_budget.service.add_members.use_case import AddMembersCommand, AddMembersUseCase
from event_budget.service.add_purchases.use_case import AddPurchaseCommand, AddPurchaseUseCase
from event_budget.service.dto import CreateEventDTO, CreateMemberDTO, CreatePurchaseDTO, TransactionDTO
from event_budget.service.get_transactions.use_case import GetTransactionsQuery, GetTransactionsUseCase


class FakeRepository(AbstractRepository):
    def __init__(self, events: list[Event]):
        super().__init__()
        self._events = set(events)

    def _add(self, event: Event) -> None:
        self._events.add(event)

    async def _get(self, event_id: UUID) -> Event | None:
        return next((e for e in self._events if e.id == event_id), None)

    async def _get_by_member_id(self, member_id: UUID) -> Event | None:
        return next((e for e in self._events if e.get_member_by_id(member_id) is not None), None)

    async def _get_by_purchase_id(self, purchase_id: UUID) -> Event | None:
        pass


class FakeUnitOfWork(AbstractUnitOfWork):
    def __init__(self):
        self.events = FakeRepository([])
        self.committed = False

    async def _commit(self):
        self.committed = True

    async def rollback(self):
        pass


@pytest.fixture
def unit_of_work() -> FakeUnitOfWork:
    return FakeUnitOfWork()


class TestAddEventCommand:
    @pytest.fixture
    def add_event_use_case(self, unit_of_work: FakeUnitOfWork) -> AddEventUseCase:
        return AddEventUseCase(uow=unit_of_work)

    async def test_add_new_event(self, add_event_use_case: AddEventUseCase):
        event = await add_event_use_case(AddEventCommand(event=CreateEventDTO(name="Party")))
        assert await add_event_use_case.uow.events.get(event.id) is not None
        assert add_event_use_case.uow.committed


class TestAddMembersCommand:
    @pytest.fixture
    def add_members_use_case(self, unit_of_work: FakeUnitOfWork, big_party: Event) -> AddMembersUseCase:
        unit_of_work.events = FakeRepository([big_party])
        return AddMembersUseCase(uow=unit_of_work)

    @pytest.fixture
    def command(self, big_party: Event) -> AddMembersCommand:
        return AddMembersCommand(
            event_id=big_party.id, members=[CreateMemberDTO(name="harry"), CreateMemberDTO(name="ron")]
        )

    @pytest.fixture
    def command_not_unique(self, big_party: Event) -> AddMembersCommand:
        return AddMembersCommand(event_id=big_party.id, members=[CreateMemberDTO(name="Alex")])

    async def test_add_members(self, add_members_use_case: AddMembersUseCase, command: AddMembersCommand):
        event = await add_members_use_case(command)
        assert event.member_count == 7
        assert add_members_use_case.uow.committed

    async def test_add_members_fail(
        self, add_members_use_case: AddMembersUseCase, command_not_unique: AddMembersCommand
    ):
        with pytest.raises(MemberNotUnique):
            event = await add_members_use_case(command_not_unique)
            assert add_members_use_case.uow.events.get(event.id) is not None
            assert not add_members_use_case.uow.committed


class TestAddPurchaseCommand:
    @pytest.fixture
    def add_purchase_use_case(self, unit_of_work: FakeUnitOfWork, big_party: Event) -> AddPurchaseUseCase:
        unit_of_work.events = FakeRepository([big_party])
        return AddPurchaseUseCase(uow=unit_of_work)

    async def test_add_new_purchase(self, add_purchase_use_case: AddPurchaseUseCase, big_party: Event):
        member = await add_purchase_use_case(
            AddPurchaseCommand(member_id=big_party.members[0].id, purchases=[CreatePurchaseDTO(name="A", price=5000)])
        )
        assert member.spend == 5300
        assert add_purchase_use_case.uow.committed

    async def test_add_new_purchase_member_not_found(self, add_purchase_use_case: AddPurchaseUseCase, big_party: Event):
        with pytest.raises(MemberNotFound):
            await add_purchase_use_case(
                AddPurchaseCommand(member_id=uuid4(), purchases=[CreatePurchaseDTO(name="A", price=5000)])
            )
            assert not add_purchase_use_case.uow.committed


class TestGetTransactionsQuery:
    @pytest.fixture
    def get_transaction_use_case(self, unit_of_work: FakeUnitOfWork, big_party: Event) -> GetTransactionsUseCase:
        unit_of_work.events = FakeRepository([big_party])
        return GetTransactionsUseCase(uow=unit_of_work)

    async def test_add_new_event(self, get_transaction_use_case: GetTransactionsUseCase, big_party: Event):
        transactions = await get_transaction_use_case(GetTransactionsQuery(event_id=big_party.id))
        expected_transactions = [
            TransactionDTO(sender=big_party.members[3].name, receiver=big_party.members[4].name, amount=1000.0),
            TransactionDTO(sender=big_party.members[0].name, receiver=big_party.members[4].name, amount=550.0),
            TransactionDTO(sender=big_party.members[0].name, receiver=big_party.members[2].name, amount=150.0),
        ]
        assert expected_transactions == transactions
