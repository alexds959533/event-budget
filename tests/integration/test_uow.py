from uuid import UUID, uuid4

import pytest
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncEngine

from event_budget.domain.model import Member
from event_budget.infrastructure.unit_of_work import SqlAlchemyUnitOfWork

pytestmark = pytest.mark.usefixtures("mappers")


async def get_count_members(engine: AsyncEngine):
    async with engine.connect() as conn:
        count_query = text("SELECT COUNT(*) FROM member")
        result = await conn.execute(count_query)
        count = result.scalar()
    return count


async def test_uow_can_retrieve_event_and_add_members(engine: AsyncEngine, inserted_event: (UUID, UUID, UUID)):
    event_id, _, _ = inserted_event
    async with SqlAlchemyUnitOfWork(engine) as uow:
        event = await uow.events.get(event_id)
        event.add_member(Member(name="Nikita", id=uuid4()))
        await uow.commit()
    cnt_members = await get_count_members(engine)
    assert cnt_members == 2


async def test_rolls_back_uncommitted_work_by_default(engine, inserted_event: (UUID, UUID, UUID)):
    event_id, _, _ = inserted_event
    async with SqlAlchemyUnitOfWork(engine) as uow:
        event = await uow.events.get(event_id)
        event.add_member(Member(name="Nikita", id=uuid4()))
    cnt_members = await get_count_members(engine)
    assert cnt_members == 1


async def test_rolls_back_on_error(engine, inserted_event: (UUID, UUID, UUID)):
    class MyException(Exception):
        pass

    event_id, _, _ = inserted_event
    with pytest.raises(MyException):
        async with SqlAlchemyUnitOfWork(engine) as uow:
            event = await uow.events.get(event_id)
            event.add_member(Member(name="Nikita", id=uuid4()))
            raise MyException()
    cnt_members = await get_count_members(engine)
    assert cnt_members == 1
