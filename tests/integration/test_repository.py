import uuid

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from event_budget.domain.model import Event
from event_budget.infrastructure import repository

pytestmark = pytest.mark.usefixtures("mappers")


async def test_repository_add(big_party: Event, session: AsyncSession):
    repo = repository.SqlAlchemyRepository(session)
    repo.add(big_party)
    db_instance = await repo.get(big_party.id)
    assert db_instance == big_party


@pytest.mark.asyncio
async def test_repository_get(session: AsyncSession, inserted_event: (uuid.UUID, uuid.UUID, uuid.UUID)):
    event_id, _, _ = inserted_event
    repo = repository.SqlAlchemyRepository(session)
    db_instance_1 = await repo.get(event_id)
    assert db_instance_1.id == event_id


@pytest.mark.asyncio
async def test_repository_get_by_member_id(session: AsyncSession, inserted_event: (uuid.UUID, uuid.UUID, uuid.UUID)):
    event_id, member_id, _ = inserted_event
    repo = repository.SqlAlchemyRepository(session)
    db_instance_1 = await repo.get_by_member_id(member_id)
    assert db_instance_1.id == event_id


@pytest.mark.asyncio
async def test_repository_get_by_purchase_id(session: AsyncSession, inserted_event: (uuid.UUID, uuid.UUID, uuid.UUID)):
    event_id, _, purchase_id = inserted_event
    repo = repository.SqlAlchemyRepository(session)
    db_instance_1 = await repo.get_by_purchase_id(purchase_id)
    assert db_instance_1.id == event_id
