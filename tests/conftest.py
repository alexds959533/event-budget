import uuid
from typing import AsyncGenerator

import pytest
from aioresponses import aioresponses
from async_asgi_testclient import TestClient
from fastapi import FastAPI
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine
from sqlalchemy.orm import clear_mappers, sessionmaker
from sqlalchemy.orm.session import close_all_sessions
from testcontainers.postgres import PostgresContainer

from app import app
from event_budget.domain.model import Event, Member, Purchase
from event_budget.infrastructure.orm import metadata, start_mappers
from settings import WebAppSettings


@pytest.fixture(scope="session")
def postgres_container():
    with PostgresContainer() as container:
        yield container


@pytest.fixture()
async def engine(postgres_container: PostgresContainer) -> AsyncEngine:
    postgres_dsn = str(postgres_container.get_connection_url().replace("psycopg2", "asyncpg"))
    async_engine = create_async_engine(postgres_dsn)
    async with async_engine.begin() as conn:
        await conn.run_sync(metadata.create_all, checkfirst=True)
    yield async_engine
    async with async_engine.begin() as conn:
        await conn.run_sync(metadata.drop_all)


@pytest.fixture()
def session_factory(engine: AsyncEngine) -> sessionmaker:
    session_factory = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
    yield session_factory
    close_all_sessions()


@pytest.fixture()
async def session(session_factory):
    async with session_factory() as async_session:
        yield async_session


@pytest.fixture
def mappers():
    start_mappers()
    yield
    clear_mappers()


@pytest.fixture
def big_party() -> Event:
    """budget 5000, members 5"""

    purchase_1 = Purchase(name="A", price=300, id=uuid.uuid4())
    purchase_2 = Purchase(name="B", price=800, id=uuid.uuid4())
    purchase_3 = Purchase(name="C", price=350, id=uuid.uuid4())
    purchase_4 = Purchase(name="D", price=1000, id=uuid.uuid4())
    purchase_5 = Purchase(name="E", price=600, id=uuid.uuid4())
    purchase_6 = Purchase(name="F", price=200, id=uuid.uuid4())
    purchase_7 = Purchase(name="G", price=400, id=uuid.uuid4())
    purchase_8 = Purchase(name="H", price=1350, id=uuid.uuid4())

    member_1 = Member(name="Alex", purchases=[purchase_1], id=uuid.uuid4())
    member_2 = Member(name="Kirill", purchases=[purchase_4], id=uuid.uuid4())
    member_3 = Member(name="Rama", purchases=[purchase_2, purchase_3], id=uuid.uuid4())
    member_4 = Member(name="Ivan", id=uuid.uuid4())
    member_5 = Member(name="Roma", purchases=[purchase_5, purchase_6, purchase_7, purchase_8], id=uuid.uuid4())

    return Event(
        uuid.UUID("7aa29c3c-c11d-11ed-afa1-0242ac120002"),
        name="new year",
        members=[member_1, member_2, member_3, member_4, member_5],
    )


@pytest.fixture
async def inserted_event_big(big_party: Event, session: AsyncSession, mappers: None) -> None:
    session.add(big_party)
    await session.commit()


@pytest.fixture()
async def inserted_event(session: AsyncSession) -> (uuid.UUID, uuid.UUID, uuid.UUID):
    event_id = uuid.uuid4()
    query = text("INSERT INTO event (name, id) VALUES (:name, :id)")
    values = {"name": "data_base_event", "id": event_id}
    await session.execute(query, values)

    member_id = uuid.uuid4()
    query = text("INSERT INTO member (id, name, event_id) VALUES (:id, :name, :event_id)")
    values = {"name": "data_base_member", "event_id": event_id, "id": member_id}
    await session.execute(query, values)

    entity_id = uuid.uuid4()
    query = text(
        "INSERT INTO purchase (id, name, price, member_id, exclude_members) "
        "VALUES (:id, :name, :price, :member_id, :exclude_members)"
    )
    values = {"name": "A", "id": entity_id, "member_id": member_id, "price": 10, "exclude_members": []}
    await session.execute(query, values)
    await session.commit()
    return event_id, member_id, entity_id


@pytest.fixture
def test_settings(postgres_container: PostgresContainer) -> WebAppSettings:
    postgres_dsn = str(postgres_container.get_connection_url().replace("psycopg2", "asyncpg"))
    return WebAppSettings(postgres_dsn=postgres_dsn)


@pytest.fixture
@pytest.mark.usefixtures("mappers")
def test_app(test_settings: WebAppSettings) -> FastAPI:
    return app


@pytest.fixture
async def client(test_app: FastAPI) -> AsyncGenerator[TestClient, None]:
    async with TestClient(test_app) as client:
        yield client


@pytest.fixture
def mock_session():
    with aioresponses() as mock:
        yield mock
