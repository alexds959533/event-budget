from uuid import UUID

from async_asgi_testclient import TestClient

from event_budget.domain.model import Event


async def test_get_event(client: TestClient, inserted_event: (UUID, UUID, UUID)):
    event_id, _, _ = inserted_event

    response = await client.get(
        f"/api/v1/event/{event_id}",
    )
    assert response.status_code == 200


async def test_add_event(client: TestClient, big_party: Event):
    expected_data = {
        "name": "PARTY",
        "planing_date": None,
        "description": None,
        "budget": 0.0,
        "member_count": 0.0,
        "members": [],
    }
    response = await client.post("/api/v1/event/", json={"event": {"name": "PARTY"}})
    response_data = response.json()
    response_data.pop("id")
    assert response.status_code == 200
    assert response_data == expected_data
